let initPoisson = 1;
let tailleMax = 90;
let tabPoisson = [];
let tabPoissonReverse = [];
let x = 0;
let y = 0;
let taille = 0;
let sens = 0;
let csteVitesse = 0;
let mouvement =0;
let currentColors = [];
let currentColorsReverse = [];
let valSlider;
let valDifficulte;
let start = false;
let perdu = false;
let gagne = false;
let partiePause = false;
let button;
let slider;
let nbBouton = 0;
let xmouv = 0;
let ymouv = 0;
let pas = 0;

function setup() {
  createCanvas(windowWidth, windowHeight);
  extraCanvas = createGraphics(windowWidth,windowHeight);
  extraCanvas.clear();

    buttonRejoue = createButton('Rejouer');
    buttonRejoue.attribute('disabled', '');
}

function affichePoisson(x, y, taille, r, g, b) {
  fill(r,g,b);
  noStroke();
  triangle(x-taille*1.5,y-taille*0.6,x-taille*1.5,y+taille*0.6, x,y);
  ellipse(x,y,taille*2,taille);
  fill(0);
  ellipse(x+taille/2,y,taille/4)
}

function affichePoissonReverse(x, y, taille, r, g, b) {
  fill(r,g,b);
  noStroke();
  triangle(x-taille/7,y+taille/10,x+taille*1.5,y-taille*0.6, x+taille*1.5,y+taille/1.3);
  ellipse(x,y,taille*2,taille);
  fill(0);
  ellipse(x-taille/2,y,taille/4)
}

function draw() {
  background(5, 159, 255);
  fill(200,175,5);
  rect(0,height*2/3,width,height*1/3);
  
  if(start==false && nbBouton<1) {
    button = createButton('COMMENCER');
    button.position(width*0.45, height*0.43);
    button.mousePressed(CommencerJeu);
    
      slider = createSlider(0, 255, 100);
  slider.position(width*0.48, height*0.53);
  slider.style('width', '80px');
  
  sliderDifficulte = createSlider(0, 2, 0);
  sliderDifficulte.position(width*0.45, height*0.60);
  sliderDifficulte.style('width', '80px');
    nbBouton++;
  }
  
  if(start && !perdu && !gagne) {
      // Cas où on perd
      if(j.vie < 1) {
        perdu = true;
      }

      // Cas où on gagne
      if(j.taille >= tailleMax ) {
        gagne = true;
      }

       if(tabPoisson.length <=5 && tabPoissonReverse.length <=5) {
            taille = random(0,tailleMax);
            csteVitesse = random(0,1);
            sens = random(0,100);
             if(sens <50) {
               tabPoisson.push(new Poisson(taille,csteVitesse,sens));

             } else {
               tabPoissonReverse.push(new Poisson(taille,csteVitesse,sens));
             }  
        }


          for (let i=0; i<tabPoisson.length; i++) {
           tabPoisson[i].color();
           tabPoisson[i].affiche(i);
            
            if(!partiePause) {
              
            
             if (!tabPoisson[i].move(i)) {
                 if (j.collisionPoisson(tabPoisson[i].x,tabPoisson[i].y,tabPoisson[i].taille)) {
                 if(j.taille >= tabPoisson[i].taille) {
                   j.score+=floor(tabPoisson[i].taille);
                   j.taille++;
                 } else {
                   j.vie--;
                 }
                 tabPoisson.splice(i,1);
                 currentColors.splice(i,1);
               }
             }
          }

        }

      for (let i=0; i<tabPoissonReverse.length; i++) {
          tabPoissonReverse[i].colorReverse();
          tabPoissonReverse[i].afficheReverse(i);
          if(!partiePause) {
            if(!tabPoissonReverse[i].moveReverse(i)) {
              if (j.collisionPoisson(tabPoissonReverse[i].x,tabPoissonReverse[i].y,tabPoissonReverse[i].taille)) {
               if(j.taille >= tabPoissonReverse[i].taille) {
                 j.score+=floor(tabPoissonReverse[i].taille);
                 j.taille++;
               } else {
                 j.vie--;
               }
               tabPoissonReverse.splice(i,1);
               currentColorsReverse.splice(i,1);
             }
            }     
          }
      }

    

      image(extraCanvas,0,0);
      j.affiche();
      extraCanvas.clear();
    
  } else if(perdu || gagne) {
    fill(0,255,180)
    if(perdu==true){
      fill(125,0,255);
       noStroke();
      text("C'est perdu",width*0.40, height*0.45);
    }
    if(gagne== true) {
      fill(230);
      stroke(255);
      circle(windowWidth*0.2-xmouv, windowHeight+60-ymouv, 20);
      circle(windowWidth*0.21-xmouv, windowHeight+100-ymouv, 30);
      circle(windowWidth*0.27+xmouv, windowHeight+130-ymouv, 25);
      circle(windowWidth*0.3-xmouv, windowHeight+155-ymouv, 15);
      circle(windowWidth*0.33+xmouv, windowHeight+30-ymouv, 15);
      circle(windowWidth*0.76+xmouv, windowHeight+30-ymouv, 30);
      circle(windowWidth*0.62-xmouv, windowHeight+30-ymouv, 5);
      circle(windowWidth*0.63+xmouv, windowHeight+60-ymouv, 20);
      circle(windowWidth*0.68-xmouv, windowHeight+100-ymouv, 10);
      circle(windowWidth*0.7+xmouv, windowHeight+200-ymouv, 20); 
      
      if(pas<50){
        xmouv+= 0.1;
        pas++;
      } else if(pas>=50 && pas <100) {
        xmouv-= 0.1;
        pas++;
      } else {
        xmouv = 0;
        pas = 0;
      }
      
      if(ymouv <windowHeight+220) {
        ymouv+=2;
      } else {
        ymouv =0;
      }
        fill(25,0,255);
      noStroke();
       text("C'est gagné !",width*0.40, height*0.45);
    }
    text('Score : ' + j.score, width*0.40, height*0.52);
    buttonRejoue.removeAttribute('disabled');
    buttonRejoue.position(width*0.5, height*0.54);
    buttonRejoue.mousePressed(Rejoue);
  }
  else {
    fill(0);
    textSize(12);
    text('Choisis la couleur de ton poisson', width*0.40, height*0.50);
    valSlider = slider.value();
    affichePoisson(height*0.5,height*0.55,30,(valSlider*3)%255, 10+(valSlider*5)%180,80+(valSlider*8)%90)
    text('Choisis la difficulté', width*0.42, height*0.6);
    text('Facile, moyen, difficile', width*0.42, height*0.65);
    valDifficulte =  sliderDifficulte.value();
  }
}

function CommencerJeu () {
  start = true;
  button.remove()
  slider.remove();
  sliderDifficulte.remove();
}

function Rejoue () {
  start = false;
  nbBouton = 0;
  perdu = false;
  gagne= false;
  buttonRejoue.remove();
  buttonRejoue = createButton('Rejouer');
  buttonRejoue.attribute('disabled', '');
  ymouv= 0;
  xmouv=0;
  j = new Joueur();
  
  tabPoisson = [];
  tabPoissonReverse = [];
}

class Poisson {
  constructor(taille,vitesse,sens) {
    this.taille=taille;
    this.y = random(0,windowHeight);
    this.vitesse = vitesse;
    this.sens = sens;
    
    if(this.sens >=50) {
      this.x = windowWidth+this.taille*2;
    } else if(this.sens < 50) {
      this.x = -this.taille*2;
    }
  }
  
  move(i) {
    if(this.x < windowWidth+130) {
      this.x += 1.2 +this.vitesse + (valDifficulte*1.3);
      return false;
    }
    else {
      tabPoisson.splice(i,1);
      currentColors.splice(i,1);
      this.x = -this.taille*2;
      return true;
    }
  }
  
  moveReverse(i) {
    if(this.x > -130) {
      this.x = this.x -(1.2 + this.vitesse + (valDifficulte*1.3));
      return false;
    } else {
      tabPoissonReverse.splice(i,1);
      currentColorsReverse.splice(i,1);
      this.x = 800+this.taille*2;
      return true;
    }
    
  }
  
  color() {
     currentColors.push(color(random(0,255),random(0,255),random(0,255)));
  }
  
  colorReverse() {
     currentColorsReverse.push(color(random(0,255),random(0,255),random(0,255)));
  }
  
  affiche(i) {
     extraCanvas.fill(currentColors[i]);
     extraCanvas.noStroke();
     extraCanvas.triangle(this.x-this.taille*1.5,this.y-this.taille*0.6,this.x-this.taille*1.5,this.y+this.taille*0.6, this.x,this.y);
     extraCanvas.ellipse(this.x,this.y,this.taille*2,this.taille);
     extraCanvas.fill(0);
     extraCanvas.ellipse(this.x+this.taille/2,this.y,this.taille/4)
  }
  
  afficheReverse(i) {
    extraCanvas.fill(currentColorsReverse[i]);
    extraCanvas.noStroke();
    extraCanvas.triangle(this.x-this.taille/7,this.y+this.taille/10,this.x+this.taille*1.5,this.y-this.taille*0.6, this.x+this.taille*1.5,this.y+this.taille/1.3);
    extraCanvas.ellipse(this.x,this.y,this.taille*2,this.taille);
    extraCanvas.fill(0);
   extraCanvas.ellipse(this.x-this.taille/2,this.y,this.taille/4)
  }
}

 class Joueur {
  constructor(valSlider) {
    this.x = 400;
    this.y = 300;
    this.taille = 30;
    this.score = 0;
    this.vie = 3;
    this.droite = true;
  }
   
   affiche() {
     //print(this.r);
    //print(valSlider);
     // Affiche le poisson
     if(this.droite) {
       fill((valSlider*3)%255,10+(valSlider*5)%180,80+(valSlider*8)%90);
       noStroke();
       triangle(this.x-this.taille*1.5,this.y-this.taille*0.6,this.x-this.taille*1.5,this.y+this.taille*0.6, this.x,this.y);
       ellipse(this.x,this.y,this.taille*2,this.taille);
       fill(0);
       ellipse(this.x+this.taille/2,this.y,this.taille/4)
       } else {
       fill((valSlider*3)%255,10+(valSlider*5)%180,80+(valSlider*8)%90);
      noStroke();
      triangle(this.x-this.taille/7,this.y+this.taille/10,this.x+this.taille*1.5,this.y-this.taille*0.6, this.x+this.taille*1.5,this.y+this.taille/1.3);
      ellipse(this.x,this.y,this.taille*2,this.taille);
      fill(0);
      ellipse(this.x-this.taille/2,this.y,this.taille/4)
     }
     
     //Affiche le score
     textSize(30);
     text(this.score, width/2, 35);
     fill(255,0,0);
     stroke(0);
     
     // Affiche le nombre de vie
     for(let i=0; i<this.vie; i++) {
       rect(10+30*i,10,20,20);
     }
     
     // Mise à jour de la position du poisson
     if(!partiePause) {
     if(this.x > mouseX)
       this.droite = false;
     if(this.x < mouseX)
       this.droite = true;
     this.x = mouseX;
     this.y = mouseY;
     }
   }
   
   
   
   collisionPoisson(xP,yP,tP) {
     let xminAutre = xP-tP*1.5;
     let yminAutre = yP-tP/2;
     let xmaxAutre = xminAutre+tP*2.5;
     let ymaxAutre = yminAutre + tP;
     
     let xminJoueur = this.x-this.taille*1.5;
     let yminJoueur = this.y-this.taille/2;
     let xmaxJoueur = xminJoueur+this.taille*2.5;
     let ymaxJoueur = yminJoueur + this.taille;
     
     let testX = xminJoueur <= xmaxAutre && xminAutre <= xmaxJoueur;
     let testY = yminJoueur <= ymaxAutre && yminAutre <= ymaxJoueur;
     
     
     
     return testX && testY;
   }
   
 }
j = new Joueur();

function keyPressed() {
  if(keyCode === 80) { // Appuie sur la touche P
    partiePause = !partiePause;
  }
}

