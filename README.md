# Jeu du poisson - p5.js
Projet réalisé en binôme. 

## a) Principe du jeu
Le jeu que nous avons souhaité développer est le “Jeu du Poisson” . Le joueur déplace un
poisson de petite taille au début au milieu d’autres poissons. L’objectif est de manger
d’autres poissons de taille plus petite ou égale afin de faire grossir le poisson et ainsi devenir
le plus gros et également faire grimper le score.

## b) Règles du jeu
- Le joueur commence avec un poisson de petite taille
- Le joueur à 3 vies
- Le joueur ne peut manger que des poissons de même taille ou taille inférieure
- Si le poisson essaie de manger un poisson plus gros il perd une vie
- Le poisson grossit au fur et à mesure
- Le jeu est gagné si le poisson atteint la taille maximum
- Le score s’incrémente en fonction de la taille des poissons mangés, pour avoir un
score élevé il faut manger le plus possible des poissons de grande taille lorsque c’est
possible
- Le jeu est perdu si le joueur perd toutes ses vies

## c) Fonctionnalités implémentées
Dans un premier temps nous avons créé une classe Joueur qui permet de créer le poisson
qui sera manipulé par le joueur, ce poisson possède des caractéristiques : taille, couleur,
position (mise à jour au déplacement) et aussi des caractéristiques plutôt utiles au joueur
comme le score et les vies disponibles qui sont affichés tout au long de la partie. Toujours
dans cette classe, il y a notamment la fonction d’affichage du poisson : le profil (dessin) du
poisson change selon s’il se dirige vers la droite ou la gauche ainsi que la fonction de
gestion des collisions qui vérifie si le poisson géré par le joueur entre en contact avec un
autre poisson.
La seconde classe est celle qui génère les autres poissons qui sont destinés à être mangés
ou à faire perdre le joueur, ces poissons sont stockés dans deux tableaux. Ces poissons ont
une taille aléatoire, sont placés à une hauteur aléatoire et ont un sens. Il y a deux fonctions
d’affichage, de mouvement et aussi pour la gestion des couleurs, selon le sens associé au
poisson. Celui-ci peut provenir de la droite et se diriger vers la gauche et inversement. Les
fonctions d’affichage assignent une couleur aléatoire issue du tableau de couleurs et
dessinent le poisson. Les fonctions de mouvements permettent de déplacer les poissons
d’un côté à l’autre, lorsqu’ils ont atteint l’autre côté la variable de déplacement est remise à
son état initial, les poissons ne se déplacent pas tous à la même vitesse il y a une petite
constante aléatoire ajoutée et elle peut être encore plus augmenté selon le niveau de
difficulté du jeu. Afin que le joueur ne soit jamais bloqué (poissons trop gros) les poissons

ayant fini leur parcours sont supprimés pour laisser place à d’autres poissons de tailles
différentes et avec de nouvelles caractéristiques.
Il peut y avoir au maximum 12 poissons sur la map dont 6 provenant de la gauche (premier
tableau) et 6 provenant de la droite (deuxième tableau). Les tableaux se remplissent
aléatoirement, si les deux tableaux ont moins de 6 poissons (car suppression due à une
collision ou fin de trajet) de nouveaux poissons sont ajoutés jusqu’à ne plus respecter cette
condition.
Lors d’une collision, si le poisson mangé est plus gros le joueur perd une vie, si le poisson
est plus petit la taille du poisson associé au joueur grandi d’un point et son score est
incrémenté de la taille du poisson mangé.
En début de partie, le joueur tombe sur un menu qui lui permet de choisir la couleur de son
poisson en utilisant un curseur et de visualiser en direct le rendu. Il peut également choisir le
niveau de difficulté du jeu ce curseur influe sur la vitesse des autres poissons du jeu, on
pourrait croire que la vitesse n’augmente pas beaucoup mais il est compliqué de navigué
avec un poisson de gros gabarit jusqu’à la fin du jeu. Une fois ses choix fait le joueur clique
sur le bouton commencer. Au clic sur le bouton commencer la fonction CommencerJeu()
est appelée et passe la variable start à true, supprime le bouton et les sliders. Comme la
variable start est à true le jeu se lance, les variables gagne et perdu sont à false, pour la
première variable tant que le poisson n’a pas la taille maximum (+ de 60 poissons mangés),
pour la seconde variable tant que le joueur à au moins une vie.
Lorsque l’une des variables perdu ou gagne passe à true le jeu s’arrête, on ne voit plus les
poissons et on affiche le score ainsi qu’un bouton qui permet de recommencer à jouer.
Si le jeu s’arrête parce que le joueur a gagné une animation apparaît en boucle, il s’agit de
bulles d’air qui remontent à la surface en se déplaçant verticalement et avec de petits
mouvements horizontaux aussi.
En cliquant sur le bouton recommencer, la fonction Rejoue() est appelée, les variables
nécessaires sont remises à leur état initial, on vide également les tableaux de poissons et on
recrée un joueur.
Au niveau de la navigation au clavier, il est possible de mettre le jeu sur pause en appuyant
sur la touche P, cela passe une variable à true si on est en train de jouer et cela bloque
notamment l’appel à la fonction move() qui permet au poisson de se déplacer ainsi que le
déplacement du poisson du joueur avec la souris. En appuyant de nouveau sur P la partie
reprend.

<img src="illustrations/Capture6.PNG" height="400" width="auto">
<img src="illustrations/Capture8.PNG" height="400" width="auto">
<img src="illustrations/Capture9.PNG" height="400" width="auto">


